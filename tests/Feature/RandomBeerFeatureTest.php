<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * The "random beer" feature
 * @package Tests\Feature
 */
class RandomBeerFeatureTest extends TestCase
{
	/**
	 * Test if the route actually exists
	 */
	public function testIfRouteExists()
    {
	    // Send the request
	    $response = $this->get('randomBeer/get');

	    // Assert HTTP status 200
	    $response->assertStatus(200);
    }

	/**
	 * Checks if the response actually returns with the beer from the API
	 */
	public function testIfBeerIsFetched()
    {
	    $response = $this->get('randomBeer/get');
	    $response->assertJsonStructure(['beer' => []]);
    }
}
