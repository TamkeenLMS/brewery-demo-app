<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * The "Search" feature tests
 * @package Tests\Feature
 */
class SearchFeatureTest extends TestCase
{
	use WithoutMiddleware;

	/**
	 * @param array $data
	 *
	 * @return \Illuminate\Foundation\Testing\TestResponse
	 */
	public function sendRequest(array $data = []){
		return $this->post('search', $data);
	}

    /**
     * Tests if the search route does exist in the first place
     *
     * @return void
     */
    public function testIfRouteExists()
    {
	    $response = $this->post('search');
	    $response->assertStatus(200);
    }

	/**
	 * Tests if the validation triggers
	 */
	public function testIfValidationTriggers(){
		$response = $this->sendRequest();
		$response->assertJsonStructure(['validation_errors']);
	}

	/**
	 * Tests if the validation rule for the name being short works
	 */
	public function testIfSentNameIsShort(){
		$response = $this->sendRequest(['name' => 'ab'])->json();

		// Assert that the "name" input is on the list of inputs with errors
		$this->assertArrayHasKey('name', $response['validation_errors']['all']);

		$this->assertContains('must be at least', $response['validation_errors']['all']['name'][0]);
	}

	/**
	 * Tests the validation rule for when the passed name isn't valid
	 */
	public function testIfSentNameIsInvalid(){
		$response = $this->sendRequest(['name' => 'abc_'])->json();

		$this->assertContains('format is invalid.', $response['validation_errors']['all']['name'][0]);
	}

}
