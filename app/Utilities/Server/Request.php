<?php namespace App\Utilities\Server;

	/**
	 * A simple extra layer on top, just to help with small repetitive code and with handling validation. This and
	 * \Server\Response both will help standardize the response content and make pushing responses quicker.
	 *
	 * @package App\Utils\Server
	 */
	class Request{
		/**
		 * The response instance
		 * @var Response
		 */
		public $response;

		/**
		 * @param Response $response
		 */
		public function linkResponse(Response $response){
			$this->response = $response;
		}

		/**
		 * Tells if the request is valid, if not the validation errors will be passed along with the response
		 * @param array $rules
		 * @param array $customAttributes
		 * @param array $customMessages
		 *
		 * @return bool
		 */
		public function validate(array $rules = [], $customAttributes = [], $customMessages = []){
			// Validate
			$validator = \Validator::make(request()->toArray(), $rules, [], $customAttributes);

			// Custom messages ?
			if($customMessages){
				$validator->setCustomMessages($customMessages);
			}

			// Failed?
			if($validator->fails()){
				$errorsArray = $validator->errors()->toArray();

				// Create an array of the error messages, not grouped by the input
				$errorMessages = [];
				foreach($errorsArray as $input => $errors){
					foreach($errors as $error){
						$errorMessages[] = $error;
					}
				}

				$this->response->with('validation_errors', [
					'messages' => $errorMessages,
					'all' => $errorsArray
				]);

				return false;
			}

			return true;
		}
	}