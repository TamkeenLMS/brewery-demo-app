<?php namespace App\Utilities\Server;

	use Illuminate\Support\Collection;

	/**
	 * A simple extra layer to help with standardizing the response for the AJAX requests.
	 * @package App\Utils
	 */
	class Response extends \Illuminate\Http\Response{
		/**
		 * The content of the response
		 * @var Collection
		 */
		public $body;

		/**
		 * @param array $data
		 * @param int $status
		 * @param array $headers
		 */
		public function __construct(array $data = [], $status = 200, array $headers = []){
			parent::__construct('', $status, $headers);

			// Create a collection of the passed data
			$this->body = collect($data);
		}

		/**
		 * Creates a new instance of Response
		 * @param array $data
		 * @param int $status
		 * @param array $headers
		 *
		 * @return Response
		 */
		public static function createNew(array $data = [], $status = 200, array $headers = []){
			return new self($data, $status, $headers);
		}

		/**
		 * Data to pass in the response
		 * @param $key
		 * @param null $value
		 *
		 * @return Response
		 */
		public function with($key, $value = null){
			// If it's a collection
			if($key instanceof Collection){
				$key = $key->toArray();
			}

			if(is_array($key)){
				$this->body = $this->body->merge($key);

			}else{
				$this->body->offsetSet($key, $value);
			}

			// Set the content of the response, every time this method is called
			$this->setContent($this->body->toArray());

			return $this;
		}

		/**
		 * Picks certain values to pass, from a given array or collection
		 * @param string $key [optional]
		 * @param array $data The data to pick from
		 * @param array $needed The keys of the indexes needed
		 *
		 * @return Response
		 */
		public function passOnly($key = null, $data, array $needed){
			if(!$data instanceof Collection){
				$data = collect($data);
			}

			if(empty($key)){
				return $this->with($data->only($needed));

			}else{
				return $this->with($key, $data->only($needed));
			}
		}

		/**
		 * Passes all data passed, except for certain values
		 * @param string $key [optional]
		 * @param $data
		 * @param array $needed
		 *
		 * @return Response
		 */
		public function passExcept($key = null, $data, array $notNeeded){
			if(!$data instanceof Collection){
				$data = collect($data);
			}

			if(empty($key)){
				return $this->with($data->except($notNeeded));

			}else{
				return $this->with($key, $data->except($notNeeded));
			}
		}

		/**
		 * @param $message
		 *
		 * @return Response
		 */
		public function withMessage($message){
			return $this->with('message', $message);
		}

		/**
		 * @param $error
		 *
		 * @return Response
		 */
		public function withError($error){
			return $this->with('error', $error);
		}

		/**
		 * @return Response
		 */
		public function withNotFound(){
			return $this->withError('not_found');
		}

		/**
		 * @return Response
		 */
		public function withNotAllowed(){
			return $this->withError('not_allowed');
		}

		/**
		 * @return Response
		 */
		public function withFailure(){
			return $this->withError('failed');
		}

		/**
		 * @return Response
		 */
		public function withSuccess(){
			return $this->withMessage('success');
		}

		/**
		 * Returns a response with failure or with success, based on the case/result provided
		 * @param $case
		 *
		 * @return Response
		 */
		public function withResultOf($case){
			return $case === true ?$this->withSuccess() :$this->withFailure();
		}
	}