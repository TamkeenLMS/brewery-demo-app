<?php namespace App\Http\Controllers;

	/**
	 * Home controller.
	 * Just so that we could call our SPA
	 * @package App\Http\Controllers
	 */
	class HomeController extends Controller{
		/**
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index(){
			return view('interface');
		}
	}