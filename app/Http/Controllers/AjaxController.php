<?php namespace App\Http\Controllers;

	use App\Utilities\Server\Request;
	use App\Utilities\Server\Response;

	/**
	 * @package App\Http\Controllers
	 */
	class AjaxController extends Controller{
		/**
		 * @var Request
		 */
		public $request;
		/**
		 * @var Response
		 */
		public $response;

		/**
		 */
		public function __construct(){
			parent::__construct();

			// Extra layers on top of the framework's Request and Response. Just to help create more standard
			// response and to help with repetitive code, like in the case of validation.
			$this->request = new Request();
			$this->response = new Response();

			// Link the request and the response
			$this->request->linkResponse($this->response);
		}
	}