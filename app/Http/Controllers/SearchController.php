<?php namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Validator;

	/**
	 * The search feature/section
	 * @package App\Http\Controllers
	 */
	class SearchController extends AjaxController{
		/**
		 * Sends a search request to the API
		 * @param Request $request
		 *
		 * @return \App\Utilities\Server\Response
		 */
		public function search(Request $request){
			$isValid = $this->request->validate([
				'name' => 'required|regex:/(^[A-Za-z0-9 -]+$)+/|min:3',
				'group' => 'required|in:beer,brewery',
				'pageNumber' => 'numeric|nullable'
			]);

			// If not valid
			if(!$isValid){
				return $this->response;
			}

			try{
				// Send the search request
				$results = app()->get('BreweryAPI')
					->search($request->name, $request->group, $request->pageNumber);

				// Send back the results
				return $this->response->with('results', $results);

			}catch (\Exception $exception){
				return $this->response->withError($exception->getMessage());
			}
		}
	}