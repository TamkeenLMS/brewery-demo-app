<?php namespace App\Http\Controllers;

	/**
	 * The controller for the random beer section of the user interface
	 * @package App\Http\Controllers
	 */
	class RandomBeerController extends AjaxController{
		/**
		 * Returns a random beer
		 * @return \App\Utilities\Server\Response
		 */
		public function get(){
			try{
				// Call the brewery API
				return $this->response->with('beer', app('BreweryAPI')->getRandomBeer());

			}catch (\Exception $exception){
				return $this->response->withError($exception->getMessage());
			}
		}

		/**
		 * Responds with a list of the beers the specified brewery provides
		 * @param $breweryId The id of the brewery
		 *
		 * @return \App\Utilities\Server\Response
		 */
		public function getMoreBeerByBrewery($breweryId){
			try{
				// Call the brewery API
				return $this->response->with('beers',
					app()->get('BreweryAPI')->getBreweryBeers($breweryId));

			}catch (\Exception $exception){
				return $this->response->withError($exception->getMessage());
			}
		}
	}