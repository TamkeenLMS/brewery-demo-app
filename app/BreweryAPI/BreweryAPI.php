<?php namespace App\BreweryAPI;

	use GuzzleHttp\Client;

	/**
	 * A simple interface for the Brewery DB API. This is limited to GET requests by design, but of course it could
	 * be altered to handle all types of request, but I will leave it this way since we only need couple of simple
	 * GET requests, and we are only retrieving data and not submitting, updating or deleting!
	 *
	 * Also I know they offer a PHP library for their service, but it's kinda old and I wanted to go the extra mile.
	 * @package App\BreweryAPI
	 */
	class BreweryAPI{
		/**
		 * The API key
		 * @var
		 */
		public $key;
		/**
		 * The base URL for the requests
		 */
		const BASE_URL = 'http://api.brewerydb.com/v2/';
		/**
		 * The HTTP request method
		 */
		const REQUEST_METHOD = 'GET';
		/**
		 * The format of the response
		 */
		const RESPONSE_FORMAT = 'json';

		/**
		 * @param $key
		 */
		public function __construct($key){
			$this->key = $key;
		}

		/**
		 * Sends a new request to the API
		 * @param $endpoint The endpoint targeted in the request
		 * @param array $arguments [optional] The arguments to send along with the request
		 *
		 * @return array
		 * @throws \Exception
		 */
		public function newRequest($endpoint, array $arguments = []){
			// The request arguments
			$arguments['key'] = $this->key;
			$arguments['format'] = static::RESPONSE_FORMAT;

			// The request
			try{
				$requestClient = new Client([
					'base_uri' => static::BASE_URL
				]);

				// The request
				$response = $requestClient->request(static::REQUEST_METHOD, $endpoint, [
					'verify' => false,
					'query' => $arguments,
				]);

				// Get the response
				$responseBody = $response->getBody()->getContents();

				// Decode the response
				$responseBody = \GuzzleHttp\json_decode($responseBody, true);

				// On error
				if($responseBody['status'] != 'success'){
					throw new \Exception($responseBody['message']);
				}

				// Decode and return
				return $responseBody;

			}catch (\Exception $exception){
				throw new \Exception('BreweryDB API request error: ' . $exception->getMessage());
			}
		}

		/**
		 * Requests a random beer
		 * @return array
		 * @throws \Exception
		 */
		public function getRandomBeer(){
			$response = $this->newRequest('beer/random', [
				'withBreweries' => 'Y'
			]);

			return $response['data'];
		}

		/**
		 * Returns all the beer by the specified brewery (by id)
		 * @param $breweryId
		 *
		 * @return array
		 * @throws \Exception
		 */
		public function getBreweryBeers($breweryId){
			$response = $this->newRequest('brewery/' . $breweryId . '/beers');

			return $response['data'];
		}

		/**
		 * Performs a search
		 * @param $name The name to search with
		 * @param $type The type of information to search in
		 *
		 * @return array
		 * @throws \Exception
		 */
		public function search($name, $type, $pageNumber = 1){
			return $this->newRequest('search', [
				'q'     => $name,
				'type'  => $type,
				'p'     => $pageNumber
			]);
		}
	}