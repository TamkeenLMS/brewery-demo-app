<?php

namespace App\Providers;

use App\BreweryAPI\BreweryAPI;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    // Create a singleton for the BreweryAPI
	    $this->app->singleton('BreweryAPI', function(){
		    return new BreweryAPI(env('BREWERY_DB_KEY'));
	    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
