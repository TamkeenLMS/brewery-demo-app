
    /**
     * The "random beer" section controller
     * **/
    application.controller('RandomBeerController', ['$scope', 'serverRequest', 'dialogs', function(scope, serverRequest, dialogs){
        // The beer object
        scope.beer = null;

        // Marks whether this random beer is currently being fetched from the backend or it has already been loaded
        scope.fetching = false;

        // Get the random beer
        scope.fetch = function(){
            scope.fetching = true;

            serverRequest.get('randomBeer/get').send(function(response){
                scope.fetching = false;

                // On Error
                if(response.error){
                    console.error(response.error);
                    return;
                }

                var randomBeer = response.beer;

                // If the fetched beer has neither a label or a description fetch another one
                if(!randomBeer.labels || !randomBeer.description){
                    scope.fetch();
                    return;
                }

                scope.beer = randomBeer;
            });
        };

        /**
         * Views more beer by the current random beer's brewery
         * **/
        scope.viewMoreBeer = function(){
            dialogs.open('moreBeerByBrewery.html', {'size': 'lg'}, ['$scope', '$uibModalInstance', function(modalScope, modal){
                // The brewery
                modalScope.brewery = scope.beer.breweries[0];

                // The beers
                modalScope.beers = null;

                // Load the beers
                serverRequest.get('randomBeer/getMoreBeer/' + modalScope.brewery.id).send(function(response){
                    if(response.error){
                        console.error(response.error);

                        modalScope.close();
                        return;
                    }

                    // Pass the retrieved data
                    modalScope.beers = response.beers;
                });

                /**
                 * Closes the modal
                 * **/
                modalScope.close = function(){
                	modal.close();
                }
            }]);
        };

        // Start by fetching a random beer
        scope.fetch();
    }]);