
    /**
     * The section section controller
     * **/
    application.controller('SearchController', ['$scope', 'serverRequest', function(scope, serverRequest){
        scope.search = {
            // The params set by the form
            'params': {
                'name': 'happy',
                'group': 'beer'
            },

            'validationErrors': null, // Any validation errors returned
            'searching': false, // Search in progress?!
            'results': null, // The results
            'selectedGroup': null, // Stores the selected group in which the search was conducted

            /**
             * Submits the search to the API
             * @param pageNumber
             * **/
            'submit': function(pageNumber){
                // Clear first
                this.searching = true;
                this.validationErrors = null;
                this.results = null;

                // The request params
                var params = angular.copy(this.params);
                params.pageNumber = pageNumber;

                // Do the search
                serverRequest.post('search', params).send(function(response){
                    // Done searching!
                    scope.search.searching = false;

                    // Validation
                    if(response.validation_errors){
                        scope.search.validationErrors = response.validation_errors;
                        return;
                    }

                    // On error
                    if(response.error){
                        console.error(response.error);
                        return;
                    }

                    scope.search.selectedGroup = params.group;
                    scope.search.results = response.results;
                });
            },

            /**
             * When moving between results pages. This simply re-submits the request but with the new page number
             * */
            'paginate': function(pageNumber){
                scope.search.submit(pageNumber);
            }
        };
    }]);