
    /**
     * A module to help with the communication with the server.
     * **/
    var serverRequestModule = angular.module('ServerRequestModule', []);

    /**
     * Let it be available in the root scope to know whenever a request is in progress or not
     * **/
    serverRequestModule.run(['$rootScope', function (rootScope) {
        rootScope.requestInProgress = false;
    }]);

    /**
     * The setup
     * **/
    serverRequestModule.service('serverRequestSetup', [function () {
        return {
            'setup': {
                'baseUrl': null,
                'CSRFToken': null,
                'requestOptions': {
                    'dataType': 'json',
                    'timeout': 30000
                },

                'globalSuccessCallback': null,
                'globalFailureCallback': null
            },

            'configure': function (setup) {
                this.setup = angular.merge(this.setup, setup);
            }
        }
    }]);

    /**
     * The factory, which will create a new object for the request class
     * **/
    serverRequestModule.factory('serverRequest', ['serverRequestSetup', '$rootScope', '$timeout', function (serverRequestSetup, rootScope, timeout) {
        // The request object
        var Request = function () {
            this.method = null;
            this.url = null;
            this.data = {};
            this.options = {};
        };

        /**
         * Creates a new request
         * @param method The method to use for the request; get, post, ...
         * @param url The url to send the request to
         * @param data [optional] The data to send along with the request
         * @param options [optional] Options for jQuery.ajax
         * **/
        Request.prototype.create = function (method, url, data, options) {
            var baseUrl = serverRequestSetup.setup.baseUrl;

            url = url.replace(baseUrl, '');

            this.method = method;
            this.url = baseUrl + url;
            this.data = data || {};
            this.options = options || {};

            return this;
        };

        /**
         * Creates a POST-method request
         *
         * @param url The url to send the request to
         * @param data [optional] The POST data
         * @param options [optional] Any options to pass to jQuery.ajax
         * **/
        Request.prototype.post = function (url, data, options) {
            return this.create('post', url, data, options);
        };

        /**
         * Creates a GET-method request
         *
         * @param url The url to send the request to
         * @param data [optional] The query to send along
         * @param options [optional] Any options to pass to jQuery.ajax
         * **/
        Request.prototype.get = function (url, data, options) {
            return this.create('get', url, data, options);
        };

        /**
         * This actually sends the request.
         *
         * @param successCallback [optional] A success callback. This is passed the response payload
         * @param failureCallback [optional] A failure callback. This is passed: jqXHR, textStatus, errorThrown
         * @param alwaysCallback [optional] A callback to trigger regardless of the request result. This is also
         * passed: jqXHR, textStatus, errorThrown
         * **/
        Request.prototype.send = function (successCallback, failureCallback, alwaysCallback) {
            // Mark that the request is in progress now
            rootScope.requestInProgress = true;

            var setup = serverRequestSetup.setup;

            if (!this.method || !this.url) {
                console.error('You need to initiate a request first, using "create"!');
                return false;
            }

            // The token
            if (this.data instanceof FormData) {
                this.data.append('_token', setup.CSRFToken);

                this.options.cache = false;
                this.options.processData = false;
                this.options.contentType = false;

            } else {
                this.data._token = setup.CSRFToken;
            }

            // The request options
            var options = jQuery.extend({
                'url': this.url,
                'method': this.method,
                'data': this.data

            }, this.options, setup.requestOptions);

            // Send the request
            return jQuery.ajax(options).done(function (response) {
                var globalSuccessCallbackReturn;

                timeout(function () {
                    // If a global success callback is set
                    if (setup.globalSuccessCallback) {
                        globalSuccessCallbackReturn = setup.globalSuccessCallback(response);
                    }

                    // The callback
                    if (
                        successCallback &&
                        (!setup.globalSuccessCallback || (setup.globalSuccessCallback && globalSuccessCallbackReturn === true))
                    ) {
                        successCallback(response);
                    }
                });

            }).fail(function (jqXHR, textStatus, errorThrown) {
                var globalFailureCallbackReturn;

                timeout(function () {
                    // If a global success callback is set
                    if (setup.globalFailureCallback) {
                        globalFailureCallbackReturn = setup.globalFailureCallback(jqXHR, textStatus, errorThrown);
                    }

                    // A failure callback is set?
                    if (
                        failureCallback &&
                        (!setup.globalFailureCallback || (setup.globalFailureCallback && globalFailureCallbackReturn === true))
                    ) {
                        failureCallback(jqXHR, textStatus, errorThrown);
                    }
                });

            }).always(function (jqXHR, textStatus, errorThrown) {
                timeout(function () {
                    // Mark that the request is no longer in progress
                    rootScope.requestInProgress = false;
                });

                // A "finish" callback is set?
                if (alwaysCallback) {
                    timeout(function () {
                        alwaysCallback(jqXHR, textStatus, errorThrown);
                    });
                }
            });
        };

        // Return a new instance of Request
        return new Request();
    }]);