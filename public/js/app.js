
    /**
     * Init the angular module representing the application
     * **/
    var application = angular.module('BreweryDemoApp', ['ServerRequestModule', 'ui.bootstrap']);

    // The server request module setup. This is where we provide the ServerRequestModule with the basic information
    // needed to create an Ajax request to the server
    application.run(['serverRequestSetup', function(serverRequestSetup){
        // Update the setup
        serverRequestSetup.configure({
            'baseUrl': window.App._baseUrl,
            'CSRFToken': window.App._CSRFToken,

            'globalFailureCallback': function(jqXHR, textStatus, errorThrown){
                console.error('Ajax request error: ', errorThrown);

                return false;
            }
        });
    }]);

    /**
     * The dialogs service.
     * This helps with creating UI dialogs, using Angular Bootstrap library
     * **/
    application.service('dialogs', ['$uibModal', function(modal){
        return {
            /**
             * Opens a new dialog, using $uibModal
             *
             * @param templateUrl The template url for the dialog
             * @param options The $uibModal.open options
             * @param controller The controller
             * **/
            'open': function(templateUrl, options, controller){
                // The dialog setup
                options = angular.extend({
                    'templateUrl': templateUrl,
                    'controller': controller || function(){},
                    'size': 'sm',
                    'backdrop': true,
                    'keyboard': false

                }, options || {});

                // Open the dialog
                var dialog = modal.open(options);

                dialog.result.then(function(){}, function(){});
            }
        }
    }]);

    /**
     * Simple "loading ..." icon to that can be placed whenever there's something happening in progress
     * **/
    application.directive('loadingIndicator', [function(){
        return {
            'restrict': 'E',
            'template': '<div class="loading-indicator">' +
                '<i class="icon fa fa-2x fa-spinner fa-pulse fa-fw"></i>' +
            '</div>',
            'replace': true
        }
    }]);

    /**
     * This takes an image url and either returns it or a "No image" image!
     * **/
    application.directive('thumbnail', [function(){
        return{
            'restrict': 'E',
            'scope': {'src': '=', 'alt': '='},

            'template': '<img ng-src="{{ imageUrl }}" alt="{{ alt }}" ' +
                'class="thumb img-responsive img-rounded"/>',

            'link': function(scope){
                // Watch for change in the course
                scope.$watch('src', function(newSrc, oldSource){
                    scope.imageUrl = (newSrc || window.App.url('images/no-label.png'));
                })
            }
        }
    }]);

    /**
     * Simple directive to format the "Created at" values passed along with the data
     * **/
    application.directive('createdAt', [function(){
        return {
            'restrict': 'E',
            'replace': true,
            'scope': {'beer': '=' },

            'link': function(scope, element){
                var date = scope.beer.createDate;
                if(!date) return;

                // Format then append
                element.text(
                    moment(date).format('DD/MM, YYYY')
                );
            }
        }
    }]);

    /**
     * Handles the pagination links below the content
     * **/
    application.directive('searchResultPagination', [function(){
        return {
            'restrict': 'E',
            'scope': {'data': '=', 'loader': '='},
            'templateUrl': window.App.url('html/searchResultPagination.html'),

            'link': function(scope){
                var pages = [];
                for(var i = 1; i <= scope.data.numberOfPages; i++){
                    pages.push(i);
                }

                scope.range = pages;
            }
        };
    }]);

    /**
     * A simple filter to handle string limiting
     * */
    application.filter('cut', function(){
        return function (value, max, wordwise, tail) {
            if(!value) return;

            max = parseInt(max, 10);
            if(!max) return value;
            if(value.length <= max) return value;

            value = value.substr(0, max);
            if(wordwise){
                var lastspace = value.lastIndexOf(' ');
                if(lastspace != -1){
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' ...');
        };
    });