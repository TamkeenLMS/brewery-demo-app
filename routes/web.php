<?php

	// Home
	Route::get('/', 'HomeController@index');

	// Random beer
	Route::group(['prefix' => 'randomBeer'], function(){
		Route::get('get', 'RandomBeerController@get');
		Route::get('getMoreBeer/{breweryId}', 'RandomBeerController@getMoreBeerByBrewery');
	});

	// Search
	Route::post('search', 'SearchController@search');