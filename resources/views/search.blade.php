
    <div ng-controller="SearchController" id="searchSectionHolder">
        <h2>Search</h2>

        {{--The form--}}
        @include('search.form')


        {{--Search in progress--}}
        <loading-indicator ng-if="search.searching"></loading-indicator>

        {{--Results--}}
        <div ng-if="search.results">
            @include('search.results')
        </div>
    </div>