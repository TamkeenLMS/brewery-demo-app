<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{!! Config::get('app.name') !!}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans|Bree+Serif">
    <link rel="stylesheet" href="{{ url(mix('css/app.min.css')) }}"/>
</head>
<body>

<div ng-app="BreweryDemoApp" ng-cloak id="container" class="container-fluid">
    {{--Header--}}
    <div id="header">
        <h1>Logo</h1>
    </div>

    {{--Random beer section--}}
    @include('randomBeer')

    {{--Search--}}
    @include('search')
</div>

<script src="{{ url('node_modules/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ url('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ url('node_modules/angular/angular.min.js') }}"></script>
<script src="{{ url('node_modules/moment/min/moment.min.js') }}"></script>
<script src="{{ url(mix('js/app.min.js')) }}"></script>

<script>
    window.App = {
        '_baseUrl': "{!! url('/') !!}/",
        '_CSRFToken': "{!! csrf_token() !!}",

        'url': function(path){
            return this._baseUrl + path;
        }
    }
</script>

</body>
</html>