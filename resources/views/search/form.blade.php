
	<form ng-submit="search.submit()" id="searchForm" name="searchForm" class="form-inline">
		{{--Name--}}
		<div class="form-group">
			<input ng-model="search.params.name" ng-disabled="search.searching" type="text" placeholder="Name"
			       class="form-control" required/>
		</div>

		{{--Group--}}
		<div class="form-group">
			<div class="checkbox-inline">
				<label>
					<input ng-model="search.params.group" value="beer" type="radio"/> Beer
				</label>
			</div>

			<div class="checkbox-inline">
				<label>
					<input ng-model="search.params.group" value="brewery" type="radio"/> Brewery
				</label>
			</div>
		</div>

		<div class="form-group">
			<button ng-disabled="searchForm.$invalid || search.searching" class="btn btn-primary">
                <i class="fa fa-search"></i> Search
			</button>
		</div>

        {{--Error--}}
        <div ng-if="search.validationErrors" class="form-group text-danger">
            <i class="fa fa-exclamation-triangle"></i> @{{ search.validationErrors.messages[0] }}
        </div>
	</form>
