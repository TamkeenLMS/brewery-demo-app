
    {{--No results found!--}}
    <div ng-if="!search.results.data" class="alert alert-info">
        <span class="glyphicon glyphicon-info-sign"></span> No results matches your search!
    </div>

    {{--Results found--}}
    <div ng-if="search.results.data && search.results.data.length > 0" id="resultsHolder">
        <div class="form-group pull-right">
            <input ng-model="resultsFilter" type="text" placeholder="Filter results ..."
                   class="input-sm form-control"/>
        </div>

        <h4>Results <span class="badge">@{{ search.results.totalResults }}</span></h4>

        {{--Results--}}
        {{--When the search is for breweries--}}
        <ul ng-if="search.selectedGroup == 'brewery'" id="breweryResults" class="list-group">
            <li ng-repeat="brewery in search.results.data | filter:resultsFilter " class="result list-group-item">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 text-center">
                        <thumbnail src="brewery.images.medium" alt="brewery.name"></thumbnail>
                    </div>

                    <div class="col-xs-12 col-sm-8 col-md-10 col-lg-10">
                        {{--Name--}}
                        <h3 class="name">
                            @{{ brewery.name }}

                            {{--Verification mark--}}
                            <i ng-if="brewery.status == 'verified'"
                               class="verificationMark fa fa-check-circle"></i>
                        </h3>

                        <ul ng-if="brewery.established || brewery.website" class="list-inline small">
                            <li ng-if="brewery.established">
                                Established <strong>@{{ brewery.established }}</strong>
                            </li>

                            <li ng-if="brewery.website">
                                <a href="@{{ brewery.website }}" target="_blank">
                                    <i class="fa fa-globe"></i>Website</a>
                            </li>
                        </ul>

                        {{--Description--}}
                        <div class="description">@{{ beer.description || '[No description available]' }}</div>
                    </div>
                </div>
            </li>
        </ul>

        {{--When the search is for beer--}}
        <ul ng-if="search.selectedGroup == 'beer'" id="beerResults" class="list-group">
            <li ng-repeat="beer in search.results.data | filter:resultsFilter" class="result list-group-item">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 text-center">
                        <thumbnail src="beer.labels.medium" alt="brewery.name"></thumbnail>
                    </div>

                    <div class="col-xs-12 col-sm-8 col-md-10 col-lg-10">
                        {{--Name--}}
                        <h3 class="name">
                            @{{ beer.nameDisplay }}

                            {{--Verification mark--}}
                            <i ng-if="beer.status == 'verified'"
                               class="verificationMark fa fa-check-circle"></i>
                        </h3>

                        <div class="small">
                            Under <strong>@{{ beer.style.name }}</strong>,
                            added <strong><created-at beer="beer"></created-at></strong>
                        </div>

                        {{--Description--}}
                        <div class="description">@{{ beer.description || '[No description available]' }}</div>

                    </div>
                </div>
            </li>
        </ul>

        {{--Pagination--}}
        <search-result-pagination data="search.results" loader="search.paginate"></search-result-pagination>
    </div>

