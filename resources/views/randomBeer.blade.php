
    <div ng-controller="RandomBeerController" id="randomBeerSectionHolder">
        <div class="container-fluid">
            {{--Loading ...--}}
            <loading-indicator ng-if="!beer"></loading-indicator>

            {{--Loaded--}}
            <div ng-if="beer" class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
                    {{--Beer label--}}
                    <thumbnail src="beer.labels.medium" class="beerLabel" alt="brewery.name"></thumbnail>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    {{--Title--}}
                    <h3 class="name">@{{ beer.nameDisplay }}</h3>

                    <div class="small">
                        Under <strong>@{{ beer.style.name }}</strong>,
                        added <strong><created-at beer="beer"></created-at></strong>
                    </div>

                    {{--Description--}}
                    <div class="description text-justify">
                        @{{ beer.description || '[No description available]' }}
                    </div>

                    <div>
                        {{--The "randomize" button--}}
                        <button ng-click="fetch()" ng-disabled="fetching" class="btn btn-default">
                            <i ng-if="!fetching" class="fa fa-refresh"></i>
                            <i ng-if="fetching" class="fa fa-spinner fa-pulse fa-fw"></i>
                            Randomize
                        </button>

                        {{--The "More from this brewery" button--}}
                        <button ng-if="!fetching" ng-click="viewMoreBeer()" class="btn btn-link">
                            More from this brewery
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/ng-template" id="moreBeerByBrewery.html">
        <div class="modal-header">
            <button ng-click="close()" type="button" class="close">&times;</button>
            <h4 class="modal-title">More beers by @{{ brewery.name }}</h4>
        </div>

        <div id="moreBeerByBreweryModal" class="modal-body">
            {{--Loading--}}
            <loading-indicator ng-if="!beers"></loading-indicator>

            {{--Results--}}
            <div ng-if="beers">
                <div class="well well-lg">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                            <thumbnail src="brewery.images.medium"></thumbnail>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                            <h2>@{{ brewery.name }} [@{{ brewery.statusDisplay }}]</h2>

                            <ul ng-if="brewery.established || brewery.website" class="list-inline small">
                                <li ng-if="brewery.established">
                                    Established <strong>@{{ brewery.established }}</strong>
                                </li>

                                <li ng-if="brewery.website">
                                    <a href="@{{ brewery.website }}" target="_blank">
                                        <i class="fa fa-globe"></i>Website</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div ng-repeat="beer in beers" class="beer col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="thumbnail">
                            <thumbnail src="beer.labels.medium" alt="beer.name"></thumbnail>
                            <div class="caption">
                                <h4 class="name">@{{ beer.name | cut:20 }}</h4>
                                <p class="description small">@{{ beer.description | cut:90 }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button ng-click="close()" class="btn btn-default">
                <i class="fa fa-remove"></i> Close
            </button>
        </div>
    </script>