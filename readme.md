# Installation
Start by cloning this repo by typing the following in your terminal
```
git clone https://TamkeenLMS@bitbucket.org/TamkeenLMS/brewery-demo-app.git
cd brewery-demo-app // cd to the directory
```

Then you will need to install the project dependencies for it work.
```
composer install // Install the dependencies
npm install // Install the NPM packages

cd public && npm install // Install the UI NPM packages
```

After it's done simply type:
```
php artisan serve
```

Then head to http://172.0.0.1:8000 to start the application

# Tests
To run the tests simply type
```
phpunit
// or
cd vendor/bin/
phpunit ../../tests
```