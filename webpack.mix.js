
    let mix = require('laravel-mix');

    // Combine the JS file and version them
    mix.combine([
        'public/js/ui-bootstrap/ui-bootstrap-custom-2.5.0.min.js',
        'public/js/ui-bootstrap/ui-bootstrap-custom-tpls-2.5.0.min.js',
        'public/js/modules/ServerRequestModule.js',
        'public/js/app.js',
        'public/js/controllers/RandomBeerController.js',
        'public/js/controllers/SearchController.js'

    ], 'public/js/app.min.js').version();

    // SCSS
    mix.sass('public/scss/app.scss', 'public/css');

    // Combine the css file and version them
    mix.combine([
        'public/node_modules/angular/angular-csp.css',
        'public/node_modules/bootstrap/dist/css/bootstrap.css',
        'public/css/ui-bootstrap-custom-2.5.0-csp.css',
        'public/css/app.css'

    ], 'public/css/app.min.css').version();